import React, {useState} from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import styles from "./styles";
import {ButtonGroup, IconButton} from "@material-ui/core";
import {useTranslation} from "react-i18next";
import {Link, useLocation} from 'react-router-dom'
import MenuIcon from '@material-ui/icons/Menu';
import {useDrawerContext} from "../../stores/DrawerContext";

const NavButton = props => {

    const [hover, setHover] = useState(false);
    const classes = styles();
    const location = useLocation().pathname.replace("/", "");

    return (

        <Link to={props.path ? props.path : '/home'} className={classes['`nav-buttons-group-link']}>

            <Button
                className={
                    ` ${classes['nav-buttons-group-button']}` +
                    ` ${props.name === location ? classes['nav-buttons-group-button-active'] : ''}` +
                    ` ${hover ? classes['nav-buttons-group-button-active'] : ''}`
                }

                onMouseEnter={event => setHover(true)}
                onMouseLeave={event => setHover(false)}
            >
                {props.children}
            </Button>
        </Link>

    )
};

const NavBar = props => {

    const {t} = useTranslation();
    const classes = styles();

    const {isOpen, toggle} = useDrawerContext();

    return (

        <AppBar className={classes.navbar} position="sticky">
            <Toolbar className={styles['navbar-toolbar']}>

                <ButtonGroup className={classes['nav-buttons-group']}>

                    <NavButton name={"home"} path="home">{t('home')}</NavButton>
                    <NavButton name={"gallery"} path="gallery">{t('gallery')}</NavButton>
                    <NavButton name={"about"} path="about">{t('aboutus')}</NavButton>
                    <NavButton name={"contact"} path="contact">{t('contact')}</NavButton>
                </ButtonGroup>
                <IconButton className={classes['nav-burger']} onClick={toggle}>
                    <MenuIcon style={{
                        fontSize: '1.2em'
                    }}/>
                </IconButton>
            </Toolbar>
        </AppBar>
    );
}

export default NavBar;