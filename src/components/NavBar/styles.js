import {makeStyles} from '@material-ui/core/styles';

const styles = makeStyles((theme) => ({

    navbar: {

        width: '100%',
        minWidth: '100%',
        maxWidth: '100%',

        height: '50px',
        minHeight: '50px',
        maxHeight: '50px',

        margin: 0,
        padding: 0,

        display: "inline-flex",
        justifyContent: 'center',
        flexWrap: "nowrap",
        flexGrow: 1,
    },

    'navbar-toolbar': {

        width: '100%'
    },

    'nav-buttons-group': {

        width: '100%',

        display: "inline-flex",
        justifyContent: 'center'
    },

    'nav-buttons-group-link' : {

        textDecoration : 'none !important',
    },

    'nav-buttons-group-button': {

        width: '100%',

        border: 'none',
        borderRadius: 0,

        [theme.breakpoints.down('sm')]: {

            display : 'none'
        },
    },

    'nav-buttons-group-button-active': {


        borderBottom: '1px solid #000000',
    },

    'nav-burger': {

        border: 'none',

        [theme.breakpoints.up('md')]: {

            display : 'none'
        },
    }

}));

export default styles;