import {makeStyles} from '@material-ui/core/styles';

const styles = makeStyles((theme) => ({

    paper: {

        width: '100vw',
        minWidth: '100vh',
        maxWidth: '100vw',

        height: `calc(100vh - 50px)`,
        minHeight: `calc(100vh - 50px)`,
        maxHeight: `calc(100vh - 50px)`,

        margin: 0,
        padding: 0,

        flexGrow: 1,

        background: 'rgba(255, 255, 255, 1)'
    },
}));

export default styles;