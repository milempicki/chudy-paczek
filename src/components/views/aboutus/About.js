import {Container} from "@material-ui/core";
import { withRouter } from "react-router";
import styles from "./styles";
import View from "../View";

const About = props => {

    const classes = styles();

    return (
        <View>
            About us
        </View>
    );
}

export default withRouter(About);