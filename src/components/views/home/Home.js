import View from "../View";
import {Link} from "react-router-dom";
import styles from "./styles";

const Home = props => {

    const classes = styles();

    return (
        <View>
            Home
        </View>
    );
}

export default Home;