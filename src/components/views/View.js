import {Grid, Paper} from "@material-ui/core";
import {withRouter} from "react-router";
import styles from "./styles";

const View = props => {

    const classes = styles();

    return (

        <Grid className={classes.root} container spacing={props.spacing || 0}>

            <Paper className={classes.paper}>

                {props.children}
            </Paper>
        </Grid>
    );
}

export default withRouter(View);