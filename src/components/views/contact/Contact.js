import {Container} from "@material-ui/core";
import { withRouter } from "react-router";
import styles from "./styles";
import View from "../View";

const Contact = props => {

    const classes = styles();

    return (
        <View>
            Contact
        </View>
    );
}

export default withRouter(Contact);