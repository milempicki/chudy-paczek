import React from 'react'
import {Drawer as MaterialDrawer, List, ListItem, ListItemIcon, ListItemText} from "@material-ui/core";
import {Link} from "react-router-dom";

import MenuIcon from '@material-ui/icons/Menu';
import {useDrawerContext} from "../../stores/DrawerContext";
import styles from "./styles";
import {useTranslation} from "react-i18next";

const RightDrawer = props => {

    const classes = styles();
    const {isOpen, toggle} = useDrawerContext();
    const {t} = useTranslation();

    return (

        <MaterialDrawer PaperProps={{
            background : 'red'
        }} anchor="right" open={isOpen} onClose={toggle}>

            <List className={classes['drawer-list']}>

                <Link to="/home" className={classes['drawer-list-link']}>
                    <ListItem button className={classes['drawer-list-item']}>

                        <ListItemText>{t('home')}</ListItemText>
                    </ListItem>
                </Link>
                <Link to="/gallery" className={classes['drawer-list-link']}>
                    <ListItem button className={classes['drawer-list-item']}>

                        <ListItemText>{t('gallery')}</ListItemText>
                    </ListItem>
                </Link>
                <Link to="/about" className={classes['drawer-list-link']}>
                    <ListItem button className={classes['drawer-list-item']}>

                        <ListItemText>{t('aboutus')}</ListItemText>
                    </ListItem>
                </Link>
                <Link to="/contact" className={classes['drawer-list-link']}>
                    <ListItem button className={classes['drawer-list-item']}>

                        <ListItemText>{t('contact')}</ListItemText>
                    </ListItem>
                </Link>

            </List>
        </MaterialDrawer>
    );
};

export default RightDrawer;