import {makeStyles} from '@material-ui/core/styles';

const styles = makeStyles((theme) => ({

    drawer: {

        width : '100vw',
        background: 'transparent !important',
    },

    'drawer-list': {
        background: 'transparent !important',
    },
    'drawer-list-link': {

        textDecoration : 'none',
        '&:link' : {

            color : '#000000'
        }
    },
    'drawer-list-item': {

        width: '25vw',

        background: 'transparent !important',

        paddingLeft: 0,
        paddingRight: 0,

        textAlign: 'center'
    },
}));

export default styles;