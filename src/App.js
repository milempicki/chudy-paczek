import './css/App.css';
import React from "react";
import './css/App.css';
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import {ThemeProvider} from '@material-ui/styles';
import {Container} from "@material-ui/core";
import styles from "./styles";

import NavBar from "./components/NavBar/NavBar";
import Home from './components/views/home/Home';
import Gallery from "./components/views/gallery/Gallery";
import About from "./components/views/aboutus/About";
import Contact from "./components/views/contact/Contact";

import theme from "./theme";
import RightDrawer from "./components/Drawer/RightDrawer";
import {DrawerContextProvider} from "./stores/DrawerContext";
import BackgroundSwitchService from "./services/BackgroundSwitchService";
import {assets_url} from './util';

function App() {

    const classes = styles();

    return (
        <ThemeProvider theme={theme}>

            <Router>

                <DrawerContextProvider>

                    <NavBar/>
                    <RightDrawer/>
                </DrawerContextProvider>

                <Container className={classes.root}>

                    <Switch>
                        <Route exact path="/" component={Home}/>
                        <Route exact path="/home" component={Home}/>
                        <Route exact path="/gallery" component={Gallery}/>
                        <Route exact path="/about" component={About}/>
                        <Route exact path="/contact" component={Contact}/>
                    </Switch>
                </Container>

            </Router>
        </ThemeProvider>
    );
}

export default App;
