module.exports = {

    assets_url: (path = "") => {

        return `${document.location.href}/assets/${path}`
    }
};