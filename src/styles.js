import {makeStyles} from '@material-ui/core/styles';

const styles = makeStyles((theme) => ({

    root: {

        width: '100vw',
        minWidth: '100v#Wh',
        maxWidth: '100vw',

        height: `${window.innerHeight - 50}px`,
        minHeight: `${window.innerHeight - 50}px`,
        maxHeight: `${window.innerHeight - 50}px`,

        overflow : 'hidden',

        margin: 0,
        padding: 0,

        flexGrow: 1,

        background: 'rgba(30, 30, 30, 1)',


    },
}));

export default styles;