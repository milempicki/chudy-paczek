import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from "react-router-dom";

import App from './App';
import './css/index.css';

import i18n from "i18next";
import {initReactI18next} from "react-i18next";

import English from "./translations/english";
import Polish from "./translations/polish";

i18n.use(initReactI18next).init({

    resources: {
        en: {
            translation: {
                ...English
            }
        },
        pl: {

            translation: {
                ...Polish
            }
        }
    },
    lng: "pl",
    fallbackLng: "en",

    interpolation: {
        escapeValue: false
    }
});


ReactDOM.render(
    <BrowserRouter>

        <React.StrictMode>
            <App/>
        </React.StrictMode>
    </BrowserRouter>,
    document.getElementById('root')
);
