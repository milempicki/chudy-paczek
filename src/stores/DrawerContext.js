import * as React from 'react'
import {useContext, useState} from 'react'

const defaultState = {

    open: false
};

const DrawerContent = React.createContext([defaultState, () => {
}]);

const DrawerContextProvider = props => {

    const [state, setState] = useState(defaultState);

    return (
        <DrawerContent.Provider value={[state, setState]}>
            {props.children}
        </DrawerContent.Provider>
    )
}

const useDrawerContext = () => {

    const [state, setState] = useContext(DrawerContent);

    const isOpen = state.open;

    const toggle = event => {

        return setState({...state, open: !state.open})
    };

    return {

        isOpen,
        toggle
    };
}

export {
    DrawerContent,
    DrawerContextProvider,
    useDrawerContext
}